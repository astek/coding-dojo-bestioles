package fr.astek.codingdojobestioles;

@FunctionalInterface
public interface QuadriConsumer <T extends String, U extends Integer , V, W>{

	public String accept (T s, U t, V i, W j);
}	