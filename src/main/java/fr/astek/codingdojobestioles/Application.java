package fr.astek.codingdojobestioles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalDouble;
import java.util.concurrent.Callable;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import vertebrate.Bird;
import vertebrate.Fish;
import vertebrate.Vertebrate;

public class Application {
	static long speed = 10l;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bird piouPiou = new Bird();
		Bird mario = new Bird();
		Fish nemo = new Fish();
		List<Vertebrate> animaux = new ArrayList<>();
		String[] animalNames = { "tata", "titi", "tutu", "toto" };

		animaux.add(piouPiou);
		animaux.add(mario);
		animaux.add(nemo);
		
		List<Vertebrate> animaux2 = new ArrayList<>();
		animaux2.add(piouPiou);
		animaux2.add(mario);
		animaux2.add(nemo);
		
		List<List<Vertebrate>> allAnimals = new ArrayList<>();
		allAnimals.add(animaux);
		allAnimals.add(animaux2);

		piouPiou.setName("toutou");
		nemo.setName("Poisson clown");
		mario.setName("Bross");

		System.out.println("Est ce que " + piouPiou.getName() + " vole ?" + piouPiou.isFlying());
		System.out.println("Est ce que mario vole ?" + mario.isFlying());

		piouPiou.fly();

		System.out.println("Est ce que pioupiou vole ?" + piouPiou.isFlying());
		System.out.println("Est ce que mario vole ?" + mario.isFlying());

		System.out.println("Color of " + nemo.getName() + " ?" + nemo.getColor());
		nemo.setColor("rouge");
		System.out.println("Color of " + nemo.getName() + " ?" + nemo.getColor());

		/*
		 * for (int i=0;i<animaux.size();i++) {
		 * animaux.get(i).setName("animal"+i);
		 * System.out.println(animaux.get(i).getName()); }
		 */
		// version simplifiee de la boucle ci-dessus
		for (Vertebrate x : animaux) {
			System.out.println(x.getName());
		}

		// version Java 8 / fonctionnelle de la boucle ci-dessus
		animaux.stream().forEach((x) -> {
			System.out.println(x.getName());
		});

		for (int i = 0; i < animaux.size(); i++) {
			animaux.get(i).setName(animalNames[i]);
			System.out.println(animaux.get(i).getName());
		}

		Consumer<String> consumer1 = (String param) -> System.out.println(param);
		consumer1.accept("hello");

		Consumer<String> consumer2 = param -> System.out.println(param);
		consumer2.accept("hello bis");

		Consumer<String> consumer3 = param -> System.out.println(param);
		consumer3.accept("C'est bien lambda");

		BiConsumer<String, String> consumer4 = (param1, param2) -> System.out.println(param1 + " " + param2);
		consumer4.accept("Air France", "KLM");

		BiConsumer<Integer, Integer> summer = (param1, param2) -> System.out.println(param1 + param2);
		summer.accept(5, 6);

		QuadriConsumer qC4 = (param1, param2, param3, param4) -> {
			String rS = param1 + param3;
			Integer rI = param2 + (Integer) param4;
			return rS + rI;
		};

		System.out.println(qC4.accept("a", 1, "b", 2));
		System.out.println(qC4.accept("z", 5, "d", 1));

		// La même chose en simplifié... L'idéal aurait été d'avoir un type
		// "void" pour le retour de la méthode accept et faire tenir cette
		// lambda sur une seule ligne
		QuadriConsumer<String, Integer, String, Integer> qC5 = (param1, param2, param3, param4) -> {
			System.out.println(param1 + param3 + (param2 + param4));
			return "";
		};
		qC5.accept("a", 1, "b", 2);
		qC5.accept("z", 5, "d", 1);

		Callable number = () -> 1;
		try {
			System.out.println(number.call());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BiConsumer<Integer, Integer> consummer5 = (param1, param2) -> System.out.println(param1 + param2);
		consummer5.accept(1, 2);

		BiFunction<Integer, Integer, Long> number1 = (param1, param2) -> (long) (param1 * param2);
		System.out.println(number1.apply(2, 2));
		try {
			testException("toto");
		} catch (InputErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
		}

		final BiConsumer<List<Vertebrate>, String> birdGeneratorConsumer = (param1, param2) -> {
			final Bird newBird = new Bird();
			newBird.setName(param2);
			newBird.setSpeed(Float.valueOf(speed++));
			param1.add(newBird);
		};

		for (final String name : animalNames) {
			birdGeneratorConsumer.accept(animaux, name);
		}

		Arrays.asList(BirdName.values())
			.stream()
			.map(bird -> bird.toString())
			.forEach(name -> birdGeneratorConsumer.accept(animaux, name));

		System.out.println("Number of animals: " + animaux.size());
		
		//2017/08/11
		animaux.stream()
		.filter((Vertebrate monVertebre) -> monVertebre.getName().startsWith("a"))
		.forEach(v-> System.out.println(v.getName()));
		
		// Animaux en majuscule
		animaux.stream()
		.map((Vertebrate monVertebre) -> monVertebre.getName().toUpperCase())
		.filter( monVertebreName -> monVertebreName.startsWith("A"))
		.forEach(v-> System.out.println(v));
		
		// compte les animaux commencant par A
		long var = animaux.stream()
		.map((Vertebrate monVertebre) -> monVertebre.getName().toUpperCase())
		.filter( monVertebreName -> monVertebreName.startsWith("A"))
		.count();
		
		System.out.println(var);
	
		// faire la somme des vitesses des oiseaux qui contiennent un l
		OptionalDouble var1 = animaux.stream()
		.filter( monVertebreName -> monVertebreName.getName().toUpperCase().contains("L")) // filtre les vetebres qui contiennent un L
		.filter( vb -> vb.getClass().equals(Bird.class)) // filtre les vetebres : on ne prend que les bird
		.map(vbx -> (Bird) vbx) //cast en bird
		.mapToDouble(s -> s.getSpeed()) // retrouve la vitesse
		.average();
		
		System.out.println(var1.getAsDouble());
		
		// 18 août 2017 => Version 1: compter le nombre total de caractere des noms de vertebre
		int sum = animaux.stream()
		.map((Vertebrate monVertebre) -> monVertebre.getName())
		.mapToInt(s -> s.length())
		.sum();   
		
		System.out.println("NB total car version 1 : " + sum); 
		
		// 18 août 2017 => Version 2: compter le nombre total de caractere des noms de vertebre
		String concate =  animaux.stream()
		.map(tonVertebrate -> tonVertebrate.getName())
		.reduce("",
				 (concateName, nomVertebrate) -> concateName + nomVertebrate );
		
		System.out.println("NB total car version 2 : " +concate.length());
		 
		// 18 août 2017 => Version 3: compter le nombre total de caractere des noms de vertebre
		int sum3 = animaux.stream()
		.map(tonVertebrate -> tonVertebrate.getName())
		.mapToInt(s -> s.length())
		.reduce(0,
				(sumLength,longueur)->sumLength+longueur);
		
		System.out.println("NB total car version 3 : " + sum3);
		
		// 18 août 2017 => Version 4: compter le nombre total de caractere des noms de vertebre
		int sum4 = animaux.stream()
		.mapToInt(tonVertebrate -> tonVertebrate.getName().length())
		.reduce(0,
				(sumLength,longueur)->sumLength+longueur);
		
		System.out.println("NB total car version 4 : " + sum4);
		
		// 15 sept 2017 => collect de nom avec doublons		
		List maListe1 = animaux.stream()
				.map(tonVertebrate -> tonVertebrate.getName())
				.collect(Collectors.toList());
		System.out.println("Liste 1 : " + maListe1);
		maListe1.add(15);
		System.out.println("Liste 1 : " + maListe1);
		
		// 15 sept 2017 => collect de nom sans doublons		
		List<String> maListe2 = animaux.stream()
				.map(tonVertebrate -> tonVertebrate.getName())
				.distinct()
				.collect(Collectors.toList());
		System.out.println("Liste 2 : " + maListe2);
		maListe2.add("yvan"); 
		//maListe2.add(15);  //Liste de string donc ko
		System.out.println("Liste 2 : " + maListe2);

		//maListe3;  //Liste de noms des vertebrates commencant par C
		List<String> maListe3 = animaux.stream()
				.map(tonVertebrate -> tonVertebrate.getName())
				.distinct()
				.filter( monVertebreName -> monVertebreName.startsWith("ca"))
				.map(String::toUpperCase)
				.collect(Collectors.toList());
		System.out.println("Liste 3 : " + maListe3);
		
		//La vitesse moyenne des oiseaux qui commencent par Ca
		OptionalDouble vitMoy = animaux.stream()
		.filter(vertebrate -> vertebrate.getClass().equals(Bird.class))
		.filter(vertebrate -> vertebrate.getName().startsWith("Ca"))
		.mapToDouble(vertebrate -> ((Bird) vertebrate).getSpeed())
		.average();
		
		System.out.println("Vitesse moyenne : " + vitMoy);		
				
		// 21/09 La vitesse moyenne des oiseaux qui commencent par Ca - instance of
		OptionalDouble vitMoy2 = animaux.stream()
		.filter(vertebrate -> vertebrate instanceof Bird)
		.filter(vertebrate -> vertebrate.getName().startsWith("Ca"))
		.mapToDouble(vertebrate -> ((Bird) vertebrate).getSpeed())
		.average();
		
		System.out.println("Vitesse moyenne 2 : " + vitMoy);

		// anyMatch equivalent
		long numberBirdStartsWithCa = animaux.stream()
				.filter(vertebrate -> vertebrate instanceof Bird)
				.filter(vertebrate -> vertebrate.getName().startsWith("Ca"))
				.count();
		
		boolean birdExists = false;
		
		if(numberBirdStartsWithCa > 0) {
			birdExists = true;
		}
		
		// anyMatch
		boolean birdExists2 = animaux.stream()
				.filter(vertebrate -> vertebrate instanceof Bird)
				.anyMatch(vertebrate -> vertebrate.getName().startsWith("ca"));
		
		System.out.println("Existe t-il un Bird qui commence par un 'Ca' :" +birdExists2);
		
		// noneMatch
		boolean birdNoneExists = animaux.stream()
				.filter(vertebrate -> vertebrate instanceof Bird)
				.noneMatch(vertebrate -> vertebrate.getName().startsWith("TRUC"));
		System.out.println("Il n'existe aucun Bird dont le nom commence par \"TRUC\" :"+ birdNoneExists);
		
		boolean birdAllExists = animaux.stream()
				.filter(vertebrate -> vertebrate instanceof Bird)
				.allMatch(vertebrate -> vertebrate.getName().startsWith("ca"));
		System.out.println("tous les Bird ont un nom qui commence par \"ca\" :"+ birdAllExists);
		
		// sorted tri par ordre croissant
		System.out.println ("NOM trie par ordre croissant:");
		animaux.stream()
		.map(recupNom -> recupNom.getName())
		.sorted()
		// .peek(monStream -> System.out.println ("NOM1 :")) equivalent d'un echo pour voir ou en est le stream
		.forEach(nom -> System.out.println(nom));
		
		// sorted tri par ordre decroissant
		System.out.println ("NOM trie par ordre decroissant :");
		animaux.stream()
		.map(recupNom -> recupNom.getName().toUpperCase())
		.sorted((nom1,nom2)-> nom2.compareTo(nom1))
		.forEach(nom -> System.out.println(nom));
		
		// sorted tri par ordre croissant avec plusieurs colonnes (le premier sort est écrasé par le second)
		System.out.println ("NOM trie par ordre croissant sans passer par le map() :");
		animaux.stream()
		.filter(vertebrate -> vertebrate instanceof Bird)
		.sorted((animal1,animal2)-> animal1.getName().compareTo(animal2.getName()))
		.sorted((animal1,animal2)->((Bird) animal1).getSpeed().compareTo(((Bird) animal2).getSpeed()))
		.forEach(animal -> System.out.println(animal.getName() + ' ' + ((Bird) animal).getSpeed()));
		
		/*
		// sorted tri par ordre croissant avec plusieurs colonnes (les 2 sorts sont appliqués)
		System.out.println ("NOM trie par ordre croissant par speed puis par nom :");
		animaux.stream()
		.filter(vertebrate -> vertebrate instanceof Bird)
		.sorted((animal1,animal2)-> {
			if(((Bird) animal1).getSpeed().compareTo(((Bird) animal2).getSpeed())>0){ // retourne 0 1 ou -1 ou >1
				return 1;
			}
			return animal1.getName().compareTo(animal2.getName());		
		})
		.forEach(animal -> System.out.println(animal.getName() + ' ' + ((Bird) animal).getSpeed()));

		// sorted tri par ordre croissant avec plusieurs colonnes (les 2 sorts sont appliqués)
		System.out.println ("--NOM trie par ordre croissant par nom puis par speed :");
		animaux.stream()
		.filter(vertebrate -> vertebrate instanceof Bird)
		.sorted((animal1,animal2)-> {
			if(animal1.getName().compareTo(animal2.getName())>0){ // retourne 0 1 ou -1 ou >1
				return 1;
			}
			return ((Bird) animal1).getSpeed().compareTo(((Bird) animal2).getSpeed());
		})
		.forEach(animal -> System.out.println(animal.getName() + ' ' + ((Bird) animal).getSpeed()));
		*/
		
		// chabane > sorted tri par ordre croissant avec plusieurs colonnes (les 2 sorts sont appliqués)
		System.out.println ("VERSION chabane--NOM trie par ordre croissant par nom puis par speed :");
		animaux.stream()
		.filter(vertebrate -> vertebrate instanceof Bird)
		.sorted((animal1,animal2)-> {
			if(animal1.getName().compareTo(animal2.getName()) == 0){ // Si les noms sont les mêmes 
				// alors on tri par speed
				return ((Bird) animal1).getSpeed().compareTo(((Bird) animal2).getSpeed());
			}
			// Si les noms ne sont pas les mêmes alors on tri par nom
			return animal1.getName().compareTo(animal2.getName());
		})
		.forEach(animal -> System.out.println(animal.getName() + ' ' + ((Bird) animal).getSpeed()));
	}

	public static void testException(String input) throws InputErrorException {
		if ("toto".equals(input)) {
			throw new InputErrorException("Erreur: La val toto ne peut pas etre utilisée");
		}
	}

	/*
	 * Runnable print = () -> System.out.println("Hello world"); print.run();
	 * 
	 * Thread print2 = new Thread(new Runnable() {
	 * 
	 * @Override public void run() { System.out.println("Hello world 2"); } });
	 * print2.run();
	 * 
	 * Thread print3 = new Thread(() -> System.out.println("Hello world 3"));
	 * print3.run();
	 * 
	 */
	// allAnimals.stream().flatMap(list ->
	// list.stream()).collect(Collectors.toList());
}