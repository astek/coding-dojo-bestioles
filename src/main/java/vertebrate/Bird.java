package vertebrate;

import fr.astek.codingdojobestioles.Flying;

public class Bird extends Vertebrate implements Flying {
	private boolean flying;
	//private String name;
	private float speed;
	
	
	
	public boolean isFlying(){
		return flying;
	}
	
	private void setFlying(boolean flying){
		this.flying = flying; 
	}

	public void land(){
		setFlying(false);
	}
		
	public Float getSpeed(){
		return speed;
	}
	
	public void setSpeed(Float speed){
		this.speed = speed; 
	}

	@Override
	public boolean move() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void fly() {
		setFlying(true);
	}
	
	
	/*public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}*/
	

}
