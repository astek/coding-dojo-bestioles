package vertebrate;

public class Fish extends Vertebrate {
	private boolean liveInOcean;
	private float size;
	
	
	public boolean isLiveInOcean() {
		return liveInOcean;
	}
	public void setLiveInOcean(boolean liveInOcean) {
		this.liveInOcean = liveInOcean;
	}
	
	public float getSize() {
		return size;
	}
	public void setSize(float size) {
		this.size = size;
	}
	@Override
	public boolean move() {
		// TODO Auto-generated method stub
		return false;
	} 

	
	
}
